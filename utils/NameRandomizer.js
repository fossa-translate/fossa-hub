const fs = require('fs-extra');

export default async function getName() {
  const file = await fs.readFile('./utils/Names.txt', { encoding: 'utf-8' });
  const fullNames = file.split('\n');

  console.log(`Entered ${fullNames.length ** 2} variations of names`);
  const firstNames = [];
  const lastNames = [];
  fullNames.forEach((fullName) => {
    const [firstName, lastName] = fullName.split(' ');
    firstNames.push(firstName);
    lastNames.push(lastName);
  });
  const firstName = firstNames[randomIndex(firstNames)];
  const lastName = lastNames[randomIndex(lastNames)];
  return `${firstName} ${lastName}`;
}

function randomIndex(array) {
  const min = 0;
  const max = array.length;
  return Math.floor(min + Math.random() * (max + 1 - min));
}
