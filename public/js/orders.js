let tabName = location.search.slice(1).split('&').shift().split('type=')
  .pop();

if (!tabName) {
  tabName = 'new';
}

function initTabs(name) {
  const tabs = document.querySelectorAll('#orders-menu li');
  const active = document.querySelector(`#${name}`);
  active.classList.add('li-active');

  getOrders(name);

  tabs.forEach((tab) => {
    tab.addEventListener('click', (event) => {
      changeTab(event.target);
    });
  });
}

function changeTab(node) {
  const active = document.querySelector('.li-active');
  const id = node.getAttribute('id');
  active.classList.remove('li-active');
  node.classList.add('li-active');
  const urlPath = `?type=${id}`;
  window.history.pushState({ pageTitle: id }, '', urlPath);
  tabName = id;
  getOrders(id);
}

initTabs(tabName);

async function getOrders(type) {
  const icon = document.querySelector('.refresh-icon');
  icon.classList.add('rotate');
  const response = await fetch(`/api/orders/get/${type}`);

  const json = await response.json();
  const { orders } = json;

  cache.putMany('orders', 'id', orders);

  const id = `#template-${tabName}`;
  const template = document.querySelector(id).innerHTML;
  const renderedTemplate = Mustache.render(template, { orders });
  const ordersTable = document.querySelector('#orders-table');

  ordersTable.innerHTML = renderedTemplate;
  icon.classList.remove('rotate');
}

const refreshBtn = document.querySelector('.refresh-icon');

function onClickRefreshBtn() {
  getOrders(tabName);
}

function onPageLoaded() {
  getOrders(tabName);
}

refreshBtn.addEventListener('click', onClickRefreshBtn);
document.addEventListener('DOMContentLoaded', onPageLoaded);

async function deleteOrder(id) {
  if (!id) {
    throw new Error('Cannot delete order id');
  }
}

async function sendOrderToWork(id) {
  const url = '/api/orders/status/set/await';
  const body = JSON.stringify({ id });
  const successMessage = 'Заказ добавлен в tableName';
  const errorMessage = 'Произошла ошибка.';
  await postRequest(url, { body, successMessage, errorMessage });
  await getOrders(tabName);
}

async function archiveOrder(id) {
  const url = '/api/orders/status/set/archive';
  const body = JSON.stringify({ id });
  await postRequest(url, { body });
  await getOrders(tabName);
}

async function returnOrder(id) {
  const url = '/api/orders/status/set/await';
  const body = JSON.stringify({ id });
  await postRequest(url, { body });
  await getOrders(tabName);
}

const fullDescription = new FullDescription();

function FullDescription() {
  const className = 'modal-window-background';
  let element;
  let deleteBtn;
  let id;
  let isOpened = false;

  function toggleDescription(event) {
    if (isOpened) {
      event.stopPropagation();
      if (event.target && (event.target.className === className || event.target === deleteBtn)) {
        hide();
      }
    } else {
      event.stopPropagation();
      show(id);
    }
  }

  function show(id) {
    if (isOpened) return;
    isOpened = true;
    id = id;
    const template = document.querySelector('#template-modal-window').innerHTML;
    const order = cache.get('orders', 'id', id);
    const renderedTemplate = Mustache.render(template, { ...order });
    const body = document.querySelector('body');
    element = createEl('div', renderedTemplate, {
      className,
    });

    body.appendChild(element);

    deleteBtn = element.querySelector('.modal-window-button-close');
    deleteBtn.addEventListener('click', toggleDescription);
    element.addEventListener('click', toggleDescription);
  }

  function hide() {
    if (!isOpened) return;

    isOpened = false;
    element.removeEventListener('click', toggleDescription);
    deleteBtn.removeEventListener('click', toggleDescription);
    document.querySelector('body').removeChild(element);
    element = undefined;
    deleteBtn = undefined;
    id = undefined;
    agreePopup.hide();
  }

  this.hide = hide;
  this.show = show;

  return this;
}

function onEscKeyDown(event) {
  if (event.key === 'Escape') {
    fullDescription.hide();
    agreePopup.hide();
  }
}

document.addEventListener('keydown', onEscKeyDown);
