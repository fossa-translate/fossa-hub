document.addEventListener('DOMContentLoaded', () => {
  const loginForm = document.querySelector('.login-form');
  loginForm.addEventListener('submit', async (event) => {
    const errorBlock = document.querySelector('.login-error');
    try {
      event.preventDefault();
      event.stopPropagation();

      const login = document.querySelector('[name=username]').value;
      const password = document.querySelector('[name=password]').value;
      const body = JSON.stringify({ login, password });
      const { json, response } = await postRequest('/login', { body });

      if (response.ok) {
        location.href = '/';
      } else {
        errorBlock.style.display = 'block';
        errorBlock.textContent = json.message;
      }
    } catch (error) {
      errorBlock.style.display = 'block';
      errorBlock.textContent = error.toString();
    }
  });
});
