function Cache() {
  this.storage = {};

  this.get = function get(cellName, key, value) {
    const cell = this.storage[cellName];
    for (let i = 0; i < cell.length; i += 1) {
      const entry = cell[i];
      if (entry[key] === value.toString()) return entry;
    }
    return undefined;
  };

  this.put = function put(cellName, object) {
    if (!this.storage[cellName]) {
      this.storage[cellName] = [];
    }
    this.storage[cellName].push(object);
  };

  this.putMany = function putMany(cellName, id, array) {
    if (!this.storage[cellName]) {
      this.storage[cellName] = [];
    }
    array.forEach((entry) => {
      if (!this.get(cellName, id, entry[id])) {
        this.storage[cellName].push(entry);
      }
    });
  };
}

const cache = new Cache();
