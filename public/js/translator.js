async function assignTranslator(id) {
  const url = '/api/orders/assign';
  const body = JSON.stringify({ id });

  await postRequest(url, { body });
}

async function unassignTranslator(id) {
  const url = '/api/orders/unassign';
  const body = JSON.stringify({ id });

  await postRequest(url, { body });
}

async function submitTranslation(id) {
  const fileInput = document.querySelector('[type="file"]');

  const formData = new FormData();
  formData.append('id', id);
  const files = Array.from(fileInput.files);

  files.forEach((file) => {
    formData.append('documents', file);
  });

  const url = '/api/orders/update/';
  await saveFormData(url, formData);
}

function customFileInputEvent(event) {
  const fileInput = event.target;
  const fileRow = document.querySelector('.files-row');
  fileRow.innerHTML = '';
  const array = Array.from(fileInput.files);
  array.forEach((file) => showFile(file, fileRow));
}

// todo merge with common.js, spaghetti code is not the best code
function showFile(file, fileRow) {
  const icon = getFileIcon(file.name);
  const body = `<img src="/images/file ext/${icon}.png" class="file-icon" alt=""/>`
    + `<p class="file-name">${file.name}</p>`;
  const node = createEl('div', body, { className: 'order-file' });
  fileRow.appendChild(node);
}
