function Accordion(element) {
  const debug = false;
  if (!element) {
    // todo extend to array
    element = document.querySelector('.accordion');
  }

  this.registerEvents = function registerEvents() {
    const entries = element.querySelectorAll('.accordion-entry');
    entries.forEach((entry) => {
      entry.addEventListener('click', () => {
        if (entry.getAttribute('data-accordion') === 'active') {
          this.close();
        } else {
          this.setActive(entry);
        }
      });
    });
  };

  this.setActive = function setActive(active) {
    if (debug) {
      console.info('setActive to', [active]);
    }
    if (this.active) {
      this.close();
    }
    this.active = active;
    this.open();
  };

  this.findActive = function findActive() {
    const query = '[data-accordion="active"]';
    let active = element.querySelector(query);
    if (!active) {
      active = element.querySelector('.accordion-entry');
    }
    this.setActive(active);
  };

  this.open = function open() {
    const body = this.active.querySelector('.accordion-entry-data');
    this.active.setAttribute('data-accordion', 'active');
    body.classList.add('accordion-entry-open');
    body.classList.remove('accordion-entry-close');
    if (debug) {
      console.info('opening', [body]);
    }
  };

  this.close = function close() {
    const body = this.active.querySelector('.accordion-entry-data');
    this.active.removeAttribute('data-accordion');
    body.classList.add('accordion-entry-close');
    body.classList.remove('accordion-entry-open');
    if (debug) {
      console.info('closing', [body]);
    }
  };

  this.registerEvents();
  this.findActive();
}

const accordion = new Accordion();
