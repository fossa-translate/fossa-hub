function Popup(data) {
  const time = 10000;
  const image = data.type === 'success' ? 'content_4.png' : 'content_5.png';

  const template = document.querySelector('.notification-template');
  if (!template) return;

  const node = template.cloneNode(true);
  node.classList.remove('notification-template');

  const imageNode = node.querySelector('.image-wrapper');
  imageNode.setAttribute('src', `/images/${image}`);

  const title = node.querySelector('.title');
  title.textContent = data.message;

  const root = document.querySelector('.notification-root');
  root.appendChild(node);

  const close = node.querySelector('.close-notification');

  // const intervalTime = time / 10;
  // const cancel = node.querySelector('.notification-cancel');
  // const interval = setInterval(() => {
  //   time -= intervalTime;
  //   cancel.textContent = `Отмена (${time / 1000})`;
  //   if (time === 0) {
  //     clearInterval(interval);
  //   }
  // }, intervalTime);

  // todo: find a way fix timeout declaration
  function removePopup() {
    root.removeChild(node);
    // clearInterval(interval);
    clearTimeout(timeout);
  }

  const timeout = setTimeout(removePopup, time);
  close.addEventListener('click', removePopup);
}

function createEl(tag, body, opts = {}, style) {
  const element = document.createElement(tag);
  if (opts.className) {
    element.className = opts.className;
  }
  element.innerHTML = body;
  if (style) {
    Object.entries(style).forEach((style) => {
      const [key, value] = style;
      element.style[key] = value;
    });
  }
  return element;
}

function hide(id) {
  document.getElementById(id).style.display = 'none';
}

function show(id) {
  document.getElementById(id).style.display = 'flex';
}

function printOrder(id) {
  window.open(`/api/print/${id}`);
}

async function request(url, method, opts = {}) {
  const {
    body,
    headers = { 'Content-Type': 'application/json' },
  } = opts;

  try {
    const response = await fetch(url, {
      body,
      method,
      headers,
    });
    const json = await response.json();
    new Popup(json);

    return { json, response };
  } catch (error) {
    console.error(error);
  }
}

async function putRequest(url, opts) {
  return request(url, 'PUT', opts);
}

async function postRequest(url, opts) {
  return request(url, 'POST', opts);
}

async function getRequest(url, opts) {
  return request(url, 'GET', opts);
}

function redirectToEditOrder(id) {
  location.href = `/order/edit/${id}`;
}

async function saveFormData(url, formData) {
  const { response } = await putRequest(url, {
    body: formData,
    headers: {},
  });

  return response;
}

function prepareFiles(files) {
  const savedFiles = [];
  const unsavedFiles = [];
  files.forEach((file) => {
    if (file.saved) savedFiles.push(file);
    else unsavedFiles.push(file);
  });

  return { savedFiles, unsavedFiles };
}
const files = [];
function prepareFormData() {
  const form = document.querySelector('form');
  const formData = new FormData(form);
  formData.delete('documents');
  formData.delete('expressCheck');
  formData.delete('notaryCheck');
  formData.delete('apostyleCheck');
  formData.delete('signFossa');

  const orderPrice = document.querySelector('.order-price');
  const expressCheck = document.querySelector('#expressCheck').checked === true;
  const notaryCheck = document.querySelector('#notaryCheck').checked === true;
  const apostyleCheck = document.querySelector('#apostyleCheck').checked === true;
  const signFossa = document.querySelector('#signFossa').checked === true;

  const { savedFiles, unsavedFiles } = prepareFiles(files);
  if (savedFiles.length) {
    const string = JSON.stringify(savedFiles);
    formData.append('documents', string);
  }

  unsavedFiles.forEach((file) => {
    formData.append('documents', file);
  });

  const price = orderPrice.textContent;
  formData.append('price', price);
  formData.append('expressCheck', expressCheck);
  formData.append('notaryCheck', notaryCheck);
  formData.append('apostyleCheck', apostyleCheck);
  formData.append('signFossa', signFossa);
  return formData;
}

async function saveOrder(id) {
  const form = document.querySelector('form');
  const formData = prepareFormData(id);
  const url = form.getAttribute('action');
  const response = await saveFormData(url, formData);

  if (response.ok) {
    redirectToEditOrder(id);
  }
}

async function editOrder(id) {
  const form = document.querySelector('form');
  const formData = prepareFormData(id);
  const url = form.getAttribute('action');
  await saveFormData(url, formData);
}

function calculatePrice() {
  const orderPrice = document.querySelector('.order-price');
  const pageCount = +(document.querySelector('#pageCount').value);
  const bureauPrice = +(document.querySelector('#bureauPrice').value);
  const notaryPrice = +(document.querySelector('#notaryPrice').value);
  const price = pageCount * bureauPrice + notaryPrice;

  orderPrice.textContent = price.toString();
}

function validateForm(nodes, button) {
  let isValid = true;
  nodes.forEach((node) => {
    if (!node.value) {
      isValid = false;
    }
  });

  if (!button) {
    return;
  }

  if (isValid) {
    button.removeAttribute('disabled');
  } else {
    button.setAttribute('disabled', '');
  }
}

function getFileIcon(filename) {
  const ext = filename.split('.')
    .pop();
  const imageExts = ['png', 'jpeg', 'jpg'];
  const isImage = imageExts.indexOf(ext) > -1;
  if (isImage) {
    return 'image';
  }

  const archiveExts = ['7z', 'zip', 'rar'];
  const isArchive = archiveExts.indexOf(ext) > -1;
  if (isArchive) {
    return 'archive';
  }

  if (ext === 'doc' || ext === 'docx') {
    return 'word';
  }

  if (ext === 'pdf') {
    return 'pdf';
  }

  return 'default';
}

function getFileIndex(name) {
  let fileIndex = -1;
  files.forEach((file, index) => {
    if (file.name === name) {
      fileIndex = index;
    }
  });
  return fileIndex;
}

function deleteFile(file) {
  const index = getFileIndex(file.name);
  files.splice(index, 1);

  console.debug(`File ${file.name} deleted`);
}

function registerDeleteEvent(file, element) {
  const fileRow = document.querySelector('.order-documents-list');
  const selectedFilesWrapper = document.querySelector('.order-documents-selected');

  function deleteEvent() {
    element.remove();
    deleteFile(file);
    if (!files.length) {
      selectedFilesWrapper.style.display = 'block';
      fileRow.style.display = 'none';
    }
  }

  const deleteBtn = element.querySelector('.order-document-delete');
  deleteBtn.addEventListener('click', deleteEvent);
}

function addFile(file) {
  files.push(file);

  console.debug(`File ${file.name} added`);
}

function renderFile(file, fileRow) {
  const icon = getFileIcon(file.name);
  const src = `/images/file ext/${icon}.png`;
  const body = `<img src="${src}" class="order-document-type" alt=""/>`
    + `<p>${file.name}</p>`
    + '<img src="/images/content_7.png" class="order-document-delete" alt=""/>';

  const element = createEl('label', body, { className: 'order-selected-file' });
  fileRow.appendChild(element);

  registerDeleteEvent(file, element);
  return element;
}

function updateEvents() {
  const selectedFiles = document.querySelectorAll('.order-selected-file');
  selectedFiles.forEach((selectedFile) => {
    const name = selectedFile.textContent.trim();
    const file = { name, saved: true };
    addFile(file);
    registerDeleteEvent(file, selectedFile);
  });
}

function addFiles(array) {
  array.forEach((file) => {
    const index = getFileIndex(file.name);
    if (index === -1) {
      addFile(file);
    }
  });
}

function onFileInputChanged(event) {
  event.stopPropagation();
  event.preventDefault();
  const filesInput = event.target;

  const fileRow = document.querySelector('.order-documents-list');
  const selectedFilesWrapper = document.querySelector('.order-documents-selected');

  function forEach(file) {
    const index = getFileIndex(file.name);
    if (index === -1) {
      renderFile(file, fileRow);
      selectedFilesWrapper.style.display = 'none';
      fileRow.style.display = 'flex';
    }
  }

  const array = Array.from(filesInput.files);
  array.forEach(forEach);
  addFiles(array);
  filesInput.value = '';
}

function initFileInput(shouldUpdateEvents, eventFunc = onFileInputChanged) {
  const fileInput = document.querySelector('[type="file"]');
  fileInput.addEventListener('change', eventFunc);
  if (shouldUpdateEvents) {
    updateEvents();
  }
}

function jsonFromFormData(formData) {
  const object = {};
  formData.forEach((value, key) => { object[key] = value; });
  return object;
}

async function inviteUser() {
  const url = '/invite';
  const form = document.querySelector('form');
  const formData = new FormData(form);
  const json = jsonFromFormData(formData);
  const body = JSON.stringify(json);
  await postRequest(url, { body });
}

async function sendOrderToWork(id) {
  const url = '/api/orders/status/set/translating-ready';
  const body = JSON.stringify({ id });
  const successMessage = 'Заказ добавлен в tableName';
  const errorMessage = 'Произошла ошибка.';
  await postRequest(url, { body, successMessage, errorMessage });
}
