import server from 'server/index';
import fs from 'fs-extra';
import logger from 'server/logger';

async function createFolders() {
  const createTemp = fs.mkdirp('temp');
  const createFiles = fs.mkdirp('files');
  return Promise.all([createTemp, createFiles]);
}

async function start() {
  await createFolders();
  server();
}

start().catch((error) => {
  logger.error(error);
});
