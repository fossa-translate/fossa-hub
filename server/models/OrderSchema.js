import mongoose from 'mongoose';
import moment from 'moment';
import { ORDER_TYPES } from 'server/constants';

const { Schema } = mongoose;

// todo handle CastError
// todo make id unique, handle 11000
const OrderSchema = new Schema({
  id: {
    type: Number,
    unique: true,
    dropDups: true,
  },
  type: {
    type: String,
    index: true,
    default: 'new',
    enum: ORDER_TYPES,
  },
  userId: { type: String, optional: true },
  dateFrom: { type: Date, default: moment().toDate() },
  dateTo: { type: Date, default: moment().add(2, 'hours').toDate() },
  history: [{
    type: { type: String },
    timestamp: { type: Date, default: Date.now() },
  }],
  createdAt: { type: Number, default: Date.now() },
  qualification: { type: String },
  files: [{
    length: { type: Number, default: 11 },
    translation: { type: Array, default: [] },
    original: { type: Array, default: [] },
  }],
  translation: Array,
});

// todo map && aggregate
OrderSchema.statics.find = async function find(query) {
  const orders = await this.collection.find(query).toArray();
  return orders.map((order) => {
    if (order.type === 'translating') {
      order.typeLocalized = 'Перевод';
    } else if (order.type === 'approving') {
      order.typeLocalized = 'Проверка';
    }
    return order;
  });
};

OrderSchema.statics.getId = async function getId() {
  const docs = await this.collection.find({}).sort({ _id: -1 }).limit(1).toArray();
  const doc = docs[0];
  const id = (doc && doc.id + 1) || 1;
  return id;
};

// eslint-disable-next-line func-names
OrderSchema.pre('save', async function (next) {
  this.id = OrderSchema.getId();
  next();
});

OrderSchema.statics.assign = async function assign(id, userId) {
  const update = { userId, type: 'translating' };
  return this.collection.updateOne({ id }, update);
};

OrderSchema.statics.unassign = async function assign(id) {
  const update = { userId: undefined, type: 'translating-ready' };
  return this.collection.updateOne({ id }, update);
};
// todo  set qualifications
// const qualifications = Object
//   .entries(QUALIFICATIONS)
//   .map(([key, value]) => ({
//     selected: key === order.translatorQualification,
//     name: value,
//     value: key,
//   }));
//
// order.translatorQualification = qualifications;

//
// async function prepareOrderData(id, req) {
//   const fields = [
//     'dateFrom',
//     'dateTo',
//     'name',
//     'email',
//     'phone',
//     'listDocs',
//     'additionalMaterials',
//     'expressCheck',
//     'notaryCheck',
//     'apostyleCheck',
//     'signFossa',
//     'whereyouknow',
//     'languageFrom',
//     'languageTo',
//     'bureauPrice',
//     'pageCount',
//     'notaryPrice',
//     'prepayment',
//     'translatorQualification',
//     'price',
//   ];
//
//   const files = [];
//   function map(file) {
//     files.push(file);
//   }
//
//   if (req.fields.documents) {
//     JSON.parse(req.fields.documents).map(map);
//   }
//   if (Array.isArray(req.files.documents)) {
//     Array.from(req.files.documents).map(map);
//   }
//
//   const id = getRandomHex(16);
//   const documents = await FileSystem.saveFiles(id, id, files, 'original');
//
//   const orderData = {};
//   const checkboxesList = ['expressCheck', 'notaryCheck', 'apostyleCheck', 'signFossa'];
//
//   fields.forEach((field) => {
//     if (checkboxesList.indexOf(field) > -1) {
//       const bool = req.fields[field] === 'true';
//       orderData[field] = bool;
//     } else {
//       orderData[field] = req.fields[field];
//     }
//   });
//   return {
//     ...orderData,
//     id,
//     documents,
//   };
// }
export default OrderSchema;
