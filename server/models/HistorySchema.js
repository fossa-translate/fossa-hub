import mongoose from 'mongoose';

const { Schema } = mongoose;

const HistorySchema = new Schema({
  type: { type: String },
  timestamp: { type: Date, default: Date.now() },
});

export default HistorySchema;

// const historyAssoc = [
//   'order',
//   'user',
//   'file',
// ];
//
// const historyActions = [
//   'delete',
// ];
//
// function History() {
//   this.update = async function update(action, assoc, assocId, data) {
//     const id = getRandomHex(20);
//
//     logger.info(`Updating ${assoc} history`, action, assocId);
//     if (assoc === 'user') {
//       const userData = { userId: assocId };
//       const user = await DB.users.get(userData);
//       user.history.push(id);
//       await DB.users.update(userData, user);
//     } else if (assoc === 'order') {
//       const orderData = { id: assocId };
//       const order = await DB.orders.get(orderData);
//       order.history.push(id);
//       await DB.orders.update(orderData, order);
//     } else {
//       logger.error('Cannot update history', action, assoc, assocId);
//     }
//     const date = moment();
//     await DB.history.update({ id }, {
//       date, action, assoc, data,
//     });
//     logger.info(`${assoc} history updated`, action, assocId);
//   };
//
//   // this.undo = function (action, assoc, assocId) {
//   //   const lastAction
//   // };
//   return this;
// }
