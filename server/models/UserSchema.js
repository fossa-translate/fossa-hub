import mongoose from 'mongoose';
import passwordGenerator from 'generate-password';

import { isDevelopment, getMD5Hash } from 'server/utils';
import config from 'config';
import { QUALIFICATIONS_LOCALIZED, QUALIFICATIONS } from 'server/constants';
import getName from '../../utils/NameRandomizer';
import { ValidationError } from '../Errors';

const { salt } = config;
const { Schema } = mongoose;

const UserSchema = new Schema({
  id: {
    type: String, unique: true, required: true, dropDups: true,
  },
  password: { type: String },
  avatar: { type: String, default: 'https://via.placeholder.com/140' },
  name: { type: String },
  languages: { type: Array },
  role: { type: String, default: 'translator', enum: ['translator', 'operator'] }, // todo moar roles. admin, edtior, RO, burea or global
  qualification: { type: String, default: 'novice', enum: QUALIFICATIONS },
  history: [{
    type: { type: String },
    timestamp: { type: Date, default: Date.now() },
  }],
  balance: {
    total: { type: Number, default: 0 },
    history: { type: Array, default: [] },
  },
});

UserSchema.path('id').validate((value) => {
  const regexp = new RegExp(/^[A-z0-9]+$/);
  regexp.test(value);
}, 'id must be on latin');

// UserSchema.path('languages').validate((value) => {
//   return Array.isArray(value) && value.length >= 2;
// }, 'Username must be on latin');

UserSchema.pre('save', async function preSave(next) {
  if (!this.password) {
    const password = passwordSetter(UserSchema.statics.generatePassword());
    this.password = password;
  }
  if (!this.name) {
    this.name = await getName();
  }
});

function passwordSetter(password) {
  if (!isDevelopment()) {
    return getMD5Hash(password, salt);
  }
  return password;
}

UserSchema.path('password').set(passwordSetter);

UserSchema.statics.generatePassword = function generatePassword() {
  return passwordGenerator.generate({
    length: 16,
    numbers: true,
    symbols: true,
  });
};

UserSchema.statics.validate = function validate(id, password) {
  const user = this.find({ id });
  if (!isDevelopment() && user.password === getMD5Hash(password, salt)) {
    return user;
  }
  if (isDevelopment() && user.password === password) {
    return user;
  }

  throw new ValidationError('User is not valid');
};

UserSchema.statics.updateBalance = function updateBalance() {};

// todo make make localization smart;
UserSchema.statics.find = async function find(query) {
  const users = await this.collection.find(query).toArray();
  return users.map((user) => {
    user.qualificationLocalized = QUALIFICATIONS_LOCALIZED[user.qualification];
    return user;
  });
};

export default UserSchema;
