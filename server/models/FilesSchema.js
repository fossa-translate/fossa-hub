import mongoose from 'mongoose';

const { Schema } = mongoose;

const FilesSchema = new Schema({
  length: { type: Number, default: 11 },
  translation: { type: Array, default: [] },
  original: { type: Array, default: [] },
});

export default FilesSchema;
