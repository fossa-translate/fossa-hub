import mongoose from 'mongoose';

const { Schema } = mongoose;

const BalanceSchema = new Schema({
  total: { type: Number, default: 0, required: true },
  history: { type: Array, required: true },
});

export default BalanceSchema;
