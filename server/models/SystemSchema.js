import mongoose from 'mongoose';

const { Schema } = mongoose;

const SystemSchema = new Schema({
  hash: { type: Number },
});
export default SystemSchema;
