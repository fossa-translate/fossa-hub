const fs = require('fs-extra');
const path = require('path');
const util = require('util');
const crypto = require('crypto');
const { getMD5Hash } = require('server/utils');
const logger = require('server/logger');
const { FILES_FOLDER } = require('server/constants');
const DB = require('server/mongodb');

function FileSystem(filesCollection) {
  const self = this;

  // todo: encriptiong not working!
  const encript = false;
  self.collection = filesCollection;
  self.algorithm = 'aes-256-cbc';

  self.deleteFile = async function deleteFile(fileId, file, filePath) {
    const unlink = util.promisify(fs.unlink);
    logger.debug('Deleting', file.path, file.id);
    if (!filePath) {
      filePath = path.join(file.path, file.id);
    }
    await self.collection.remove({ id: fileId });
    return unlink(filePath);
  };

  self.writeFile = async function writeFile(iv, fileName, originalPath, dest) {
    try {
      await new Promise((resolve, reject) => {
        const key = getMD5Hash(fileName);
        const destFile = path.join(dest, fileName);

        const readStream = fs.createReadStream(originalPath);
        const writeStream = fs.createWriteStream(destFile);
        if (encript) {
          const cipher = crypto.createCipheriv(self.algorithm, key, iv);
          readStream.pipe(cipher);
        }
        readStream.pipe(writeStream);

        writeStream.on('finish', resolve);
        writeStream.on('error', reject);
      });
    } catch (error) {
      logger.error(error);
    }
  };

  self.saveFile = async function saveFile(id, file, destFolder, fileId) {
    const fileInDb = await self.getFile(fileId);

    if (fileInDb) {
      logger.info(`File ${fileId} already exist`);
      return fileId;
    }

    const iv = Buffer.from(crypto.randomBytes(16));
    await self.writeFile(iv, fileId, file.path, destFolder);
    await self.deleteFile(fileId, file, file.path);

    const meta = {
      ...self.collectFileMeta(file),
      // todo: store iv is good or not ?
      iv,
      path: destFolder,
    };

    await self.collection.update({ id: fileId }, meta);
    return fileId;
  };

  self.collectFileMeta = function collectFileMeta(file) {
    const { lastModifiedDate, size, name } = file;
    const ext = path.extname(name).replace('.', '');
    const icon = self.getFileIcon(ext);
    return {
      lastModifiedDate, icon, ext, name, size,
    };
  };

  self.saveFiles = async function saveFiles(orderId, id, files, subfolder) {
    const destFolder = path.join(process.cwd(), FILES_FOLDER, subfolder, orderId);
    fs.mkdirp(destFolder);

    const order = await DB.orders.get({ id });
    let documents;
    if (order) {
      ({ documents } = order);
    }

    async function mapSave(file) {
      const fileId = self.getFileId(id, file.name);
      if (documents && documents.indexOf(fileId) > -1) {
        return fileId;
      }

      return self.saveFile(id, file, destFolder, fileId);
    }

    async function deleteMap(fileId) {
      const file = await self.getFileMeta(fileId);
      return self.deleteFile(fileId, file);
    }

    let deleteItems = [];

    function toIdMap(file) {
      return self.getFileId(id, file.name);
    }

    if (documents && files) {
      const ids = files.map(toIdMap);
      const diff = documents.filter((x) => !ids.includes(x));
      deleteItems = diff;
    }

    const deletePromises = deleteItems.map(deleteMap);
    await Promise.all(deletePromises);

    return Promise.all(files.map(mapSave));
  };

  self.getFileIcon = function getFileIcon(ext) {
    const imageExts = ['png', 'jpeg', 'jpg'];
    const isImage = imageExts.indexOf(ext) > -1;
    if (isImage) {
      return 'image';
    }

    const archiveExts = ['7z', 'zip', 'rar'];
    const isArchive = archiveExts.indexOf(ext) > -1;
    if (isArchive) {
      return 'archive';
    }

    if (ext === 'doc' || ext === 'docx') {
      return 'word';
    }

    if (ext === 'pdf') {
      return 'pdf';
    }

    return 'default';
  };

  self.getFileId = function getFileId(id, fileName) {
    return getMD5Hash(id + fileName); // we need small hashes
  };

  self.getFileMeta = async function getFileMeta(id) {
    return self.collection.get({ id });
  };

  self.getFilesMeta = async function getFilesMeta(filesIds = []) {
    return Promise.all(filesIds.map(self.getFileMeta));
  };

  self.getFile = async function getFile(fileId) {
    const fileMeta = await self.getFileMeta(fileId);

    if (!fileMeta) {
      logger.error(`File ${fileId} not found`);
      return;
    }

    const filePath = path.join(fileMeta.path, fileMeta.name);

    if (encript) {
      return new Promise((resolve, reject) => {
        try {
          let data = '';
          // todo: mb save full path to file ?;
          const key = getMD5Hash(fileMeta.name);
          const { iv } = fileMeta;
          const decipher = crypto.createDecipheriv(self.algorithm, key, iv);
          const readStream = fs.createReadStream(filePath);

          readStream.pipe(decipher);
          decipher.on('finish', () => resolve(data));
          decipher.on('error', reject);
          decipher.on('data', (chunk) => {
            data += chunk;
          });
        } catch (error) {
          logger.error(error);
        }
      });
    }
    const file = await fs.readFile(filePath);

    if (file) {
      return file;
    }
    return null;
  };
}

module.exports = FileSystem;
