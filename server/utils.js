import 'moment/locale/ru';
import crypto from 'crypto';

// const format = 'DD.MM.YYYY - HH:mm';

export function isDevelopment() {
  return process.env.NODE_ENV === 'development';
}

export function getMD5Hash(string, salt = '') {
  return crypto.createHash('md5')
    .update(string + salt)
    .digest('hex');
}

export function getRandomHex(bytes) {
  return crypto.randomBytes(bytes).toString('hex');
}

export function prepareTemplate(data = {}, layoutName) {
  const defaultData = {
    settings: {},
    applicationTitle: 'Fossa Translate',
    environment: process.env.NODE_ENV,
    pageTitle: 'Fossa Translate',
  };

  if (layoutName) {
    defaultData.settings.layout = `layouts/${layoutName}`;
  }

  if (isDevelopment()) {
    defaultData.pageTitle = `Dev ${defaultData.pageTitle}`;
  }

  const templateData = { ...defaultData, ...data };
  return templateData;
}

export function prepareOperatorTemplate(data) {
  return prepareTemplate({ isOperator: true, ...data }, 'app-layout');
}

export function prepareLoginTemplate(data) {
  return prepareTemplate(data, 'login-layout');
}

export function prepareTranslatorTemplate(data) {
  return prepareTemplate({ isTranslator: true, ...data }, 'app-layout');
}

export function isOperator(req) {
  return req.body.token || (req.session && req.session.role !== 'operator');
}
