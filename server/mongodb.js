import mongoose from 'mongoose';

import OrderSchema from 'server/models/OrderSchema';
import UserSchema from 'server/models/UserSchema';
import SystemSchema from 'server/models/SystemSchema';
import config from 'config';

const { url } = config.mongodb;

const mongodbConfig = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  keepAlive: true,
  keepAliveInitialDelay: 300000,
};

mongoose.connect(url, mongodbConfig);
export const Order = mongoose.model('Order', OrderSchema);
export const User = mongoose.model('User', UserSchema);
export const System = mongoose.model('System', SystemSchema);

export default mongoose;
