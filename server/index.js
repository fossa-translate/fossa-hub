import morgan from 'morgan';
import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import mustacheExpress from 'mustache-express';

import { nocache, errorHandler, additionalLogger } from 'server/middlewares';
import uuid from 'uuid/v4';
import session from 'express-session';
import connectMongo from 'connect-mongo';
import config from '../config';
import logger from './logger';
import { getMD5Hash, isDevelopment } from './utils';
import router from './router';
import packageJson from '../package.json';

function sessionMiddleware() {
  const MongoStore = connectMongo(session);
  const { version } = packageJson;
  const hash = getMD5Hash(version);

  const sessionConfig = {
    name: hash,
    secret: config.server.cookieSecret,
    saveUninitialized: true,
    resave: true,
    store: new MongoStore({
      url: config.mongodb.url,
      ttl: 24 * 60 * 60, // in minutes
      autoRemove: 'native',
    }),

    genid() {
      return uuid(); // use UUIDs for session IDs
    },
  };
  return session(sessionConfig);
}

export default function server() {
  const { NODE_ENV, NODE_PATH } = process.env;
  logger.info(`Running in ${NODE_ENV} configuration`);

  const app = express();
  const templatesPath = path.join(NODE_PATH, 'pages');
  app.engine('mustache', mustacheExpress(templatesPath, '.mustache'));
  app.set('views', templatesPath);
  app.set('view engine', 'mustache');
  app.set('trust proxy', 1);

  app.use(sessionMiddleware());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(express.static('public'));
  app.use(helmet());

  if (isDevelopment()) {
    app.use(morgan('tiny'));
    app.use(nocache);
    app.use(additionalLogger);
  }

  app.use(router);
  app.use(errorHandler);

  const { port } = config.server;
  app.listen(port, () => {
    logger.info('Server is running on port:', port);
  });
}
