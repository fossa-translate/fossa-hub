import { Router } from 'express';
import fs from 'fs-extra';
import path from 'path';

const url = 'http://localhost:7000/dev/';
const pagesPath = path.join(process.cwd(), '/pages/dev');
const mainPageName = 'main-dev';

const router = new Router();

async function getPagesList(pagesDir) {
  const files = await fs.readdir(pagesDir);
  return files.map((file) => (file.split('.').shift()));
}

async function devRoute(req, res) {
  const page = req.url.split('/').pop();
  const pages = await getPagesList(pagesPath);
  if (pages.indexOf(page) > -1) {
    const pagePath = path.join(pagesPath, `${page}.mustache`);
    return res.render(pagePath);
  }
  res.statusCode = 404;
  return res.send(`Dev page not found, look at ${url} for pages`);
}

function map(page) {
  return { pageName: page, url: url + page };
}

function filter(page) {
  return page.pageName !== mainPageName;
}

async function getPagesListRoute(req, res) {
  const rawPages = await getPagesList(pagesPath);
  const pages = rawPages.map(map).filter(filter);
  res.render(`dev/${mainPageName}`, { pages });
}

router.get('/dev/:page', devRoute);
router.get('/dev/', getPagesListRoute);

export default router;
