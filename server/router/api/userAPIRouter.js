import { Router } from 'express';
import logger from 'server/logger';
import { User } from 'server/mongodb';
import { NotFoundError } from '../../Errors';

const router = new Router();

router.get('/get', async (req, res, next) => {
  const { id } = req.body;
  try {
    let users;
    if (id) {
      const user = await User.findOne({ id });
      if (!user) {
        throw new NotFoundError(`User ${id} not found`);
      }
      users = [user];
    } else {
      users = await User.find({});
      if (!users.length) {
        throw new NotFoundError('Users not found');
      }
    }

    return res.send(users);
  } catch (error) {
    return next(error);
  }
});

router.put('/new', async (req, res, next) => {
  try {
    const user = new User(req.body);
    await user.save();
    const { id } = user;
    logger.info(`User was created '${id}'`);

    const message = `Переводчик '${id}' был создан`;
    const json = { type: 'success', message, user };
    return res.send(json);
  } catch (error) {
    // todo move to schema
    if (error.name === 'MongoError' && error.code === 11000) {
      logger.error(error.message);
      const message = 'User already exist!';
      const json = { type: 'error', message };
      res.status(422);
      return res.send(json);
    }
    return next(error);
  }
});

router.post('/update', async (req, res, next) => {
  try {
    const { id } = req.body;
    await User.updateOne({ id }, req.body);
    logger.info(`User was updated '${id}'`);

    res.sendStatus(200);
  } catch (error) {
    next(error);
  }
});

router.delete('/delete', async (req, res, next) => {
  try {
    const { id } = req.body;
    await User.updateOne({ id }, { role: 'banned' });
    logger.info(`User was banned '${id}'`);

    res.sendStatus(200);
  } catch (error) {
    next(error);
  }
});

export default router;
