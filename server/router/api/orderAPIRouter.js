import { Router } from 'express';
import { Order } from 'server/mongodb';
import logger from 'server/logger';
import { formidable } from 'server/middlewares';

const router = new Router();

router.get('/get/:type', async (req, res, next) => {
  try {
    const { type } = req.params;
    const orders = await Order.find({ type }).sort({ _id: 1 });

    return res.json({ orders });
  } catch (error) {
    return next(error);
  }
});

router.post('/status/set/:type', async (req, res, next) => {
  try {
    const { id } = req.body;
    const { type } = req.params;
    // todo notify user if it's unassigned and by who bureau or user;
    await Order.unassign(id);

    const message = `Заказ № ${id} добавлен к ${type}`;
    const json = { type: 'success', message };
    return res.send(json);
  } catch (error) {
    return next(error);
  }
});

router.delete('/delete', async (req, res, next) => {
  try {
    const { id } = req.body;
    const type = 'delete';
    await Order.update({ id }, { type });

    const message = `Заказ № ${id} был удален`;
    const json = { type: 'success', message };
    return res.send(json);
  } catch (error) {
    return next(error);
  }
});

router.put('/new', formidable, async (req, res, next) => {
  try {
    console.log(req.fields);

    const order = new Order(req.fields);
    await order.save();
    const { id } = order;
    logger.info(`Order with id: ${id} was saved`);

    const message = `Заказ № ${id} успешно сохранен`;
    const json = { type: 'success', message };
    return res.send(json);
  } catch (error) {
    return next(error);
  }
});

router.post('/update', formidable, async (req, res, next) => {
  try {
    const { id } = req.fields;
    const translation = req.files.documents;
    await Order.updateOne({ id }, { translation });
    logger.info(`Order with id: ${id} was updated`);

    const message = 'Заказ отправлен на проверку';
    const json = { type: 'success', message };
    return res.send(json);
  } catch (error) {
    return next(error);
  }
});

router.post('/edit/:id', formidable, async (req, res, next) => {
  try {
    const { id } = req.params;
    const update = req.params;
    await Order.updateOne({ id }, update);
    logger.info(`Order with id: ${id} was edit`);

    const message = `Заказ № ${id} успешно отредактирован`;
    const json = { type: 'success', message };
    return res.send(json);
  } catch (error) {
    return next(error);
  }
});

router.post('/assign', async (req, res, next) => {
  try {
    const { id } = req.body;
    // todo check if user has the same qualification as the order
    // todo check if user making request is the same as assigned
    // todo check if user is translator
    await Order.assign(id, req.session.userId);

    const message = `Вы работаете над заказом ${id}`;
    const json = { type: 'success', message };
    return res.send(json);
  } catch (error) {
    return next(error);
  }
});

router.post('/unassign', async (req, res, next) => {
  try {
    const { id } = req.body;
    // todo check if user is it translator || operator
    // todo check if user is it operator in bureau
    // todo check if user is assigned already to order
    await Order.unassign(id);

    const message = `Вы отклонили заказ ${id}`;
    const json = { type: 'success', message };
    return res.send(json);
  } catch (error) {
    return next(error);
  }
});

export default router;
