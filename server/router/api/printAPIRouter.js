import { Router } from 'express';
import { Order } from 'server/mongodb';

const router = new Router();

router.get('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const order = await Order.findOne({ id });
    if (!order) {
      res.statusCode = 500;
      const message = `Order with number ${id} not found`;
      return res.send({
        status: 'error',
        message,
      });
    }

    return res.render('operator/print', { order });
  } catch (error) {
    return next(error);
  }
});

export default router;
