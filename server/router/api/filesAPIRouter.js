import { Router } from 'express';

const router = new Router();

router.get('/get/:id/:fileId', async (req, res, next) => {
  try {
    const { fileId, id } = req.params;

    if (!id) {
      return next('id must be present in request');
    }

    if (!fileId) {
      return next('fileId must be present in request');
    }

    // const file = await FileSystem.getFileMeta(fileId);

    // if (!file) {
    //   return next(`File with id: ${fileId} not found in order with id: ${id}`);
    // }
    //
    // const filePath = path.join(file.path, file.id);
    return res.send('not implemented yet');
    // return res.sendFile(filePath);
  } catch (error) {
    return next(error);
  }
});

export default router;
