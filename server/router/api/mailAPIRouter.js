import { Router } from 'express';
import moment from 'moment';
import { System, User } from 'server/mongodb';
import { authorized, onlyOperatorMiddleware } from 'server/middlewares';
import { getRandomHex } from 'server/utils';
import { sendEmail } from 'server/EmailService';

const router = new Router();

async function generateInviteUrl(hash) {
  const base = 'http://192.168.43.178:7000/invite/';
  const link = base + hash;
  const expireAt = new Date(moment().add('1', 'minute'));
  await System.update({ hash }, { expireAt });
  return link;
}

router.post('/invite', authorized, onlyOperatorMiddleware, async (req, res, next) => {
  try {
    const {
      email,
      name,
      qualification,
      languages,
    } = req.body;

    const password = User.generatePassword();

    const user = {
      email,
      name,
      qualification,
      languages,
      password,
      id: email,
      status: 'unconfirmed',
    };

    const hash = getRandomHex(32);
    const inviteUrl = await generateInviteUrl(hash);

    const emailData = {
      name,
      inviteUrl,
      email,
      systemName: 'Fossa-translate', // todo move to constants or somewhere else
      subject: 'Приглашение',
      password,
    };

    const template = 'invite';
    await sendEmail(email, template, emailData);
    await User.update({ hash }, user);

    res.send({ message: `Email sent to ${email}` });
  } catch (error) {
    next(error);
  }
});

router.get('/invite/:hash', async (req, res, next) => {
  try {
    const { hash } = req.params;
    const entry = await User.find({ hash });
    if (!entry /* && entry.timestamp */) {
      return res.render('invite-expired');
    }

    req.session.userId = entry.userId;
    req.session.role = entry.role;

    return res.redirect('/translator');
  } catch (error) {
    next(error);
  }
});

export default router;
