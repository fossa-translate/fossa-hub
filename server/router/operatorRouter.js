import { Router } from 'express';
import { Order, User } from 'server/mongodb';
import { prepareOperatorTemplate } from 'server/utils';
import languagesList from '../../languages.json';

function setLang(dict, lang, obj) {
  const indexTo = dict.indexOf(lang);
  if (indexTo > -1) {
    dict[indexTo] = { name: lang, ...obj };
  } else {
    throw new Error(`Язык ${lang} не найден`);
  }
}

function prepareLangDict(langFrom, langTo) {
  if (!langFrom && !langTo) {
    // eslint-disable-next-line no-param-reassign
    [langFrom, langTo] = languagesList.defaults;
  }
  const langDict = [...languagesList.list];
  setLang(langDict, langTo, { selectedTo: true });
  setLang(langDict, langFrom, { selectedFrom: true });
  return langDict;
}

const router = new Router();

router.get('/orders', async (req, res, next) => {
  try {
    const orders = await Order.find({});

    const data = prepareOperatorTemplate({ orders });
    return res.render('operator/orders', data);
  } catch (error) {
    return next(error);
  }
});

router.get('/order/new', async (req, res, next) => {
  try {
    const languages = prepareLangDict();
    const orderId = await Order.getId();

    const order = new Order({ id: orderId });

    const data = prepareOperatorTemplate({ order, languages });
    return res.render('operator/order-new', data);
  } catch (error) {
    return next(error);
  }
});

router.get('/order/edit/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const order = await Order.findOne({ id });
    const { languageFrom, languageTo } = order;
    const languages = prepareLangDict(languageFrom, languageTo);

    const data = prepareOperatorTemplate({ order, languages });
    return res.render('operator/order-edit', data);
  } catch (error) {
    return next(error);
  }
});

router.get('/user/new', async (req, res, next) => {
  try {
    const data = prepareOperatorTemplate();
    return res.render('operator/user-new', data);
  } catch (error) {
    return next(error);
  }
});

router.get('/users', async (req, res, next) => {
  try {
    const users = await User.find({});

    const data = prepareOperatorTemplate({ users });
    return res.render('operator/users-edit', data);
  } catch (error) {
    return next(error);
  }
});

export default router;
