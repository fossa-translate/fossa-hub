import { Router } from 'express';
import { User } from 'server/mongodb';
import { authorized } from 'server/middlewares';
import { prepareLoginTemplate, isDevelopment } from 'server/utils';
import logger from '../logger';

const router = new Router();

router.get('/', authorized, async (req, res) => {
  const { role } = req.session;
  if (role === 'operator') {
    return res.redirect('/orders');
  }
  if (role === 'translator') {
    return res.redirect('/translator');
  }
  return res.redirect('/orders');
});

router.get('/login', async (req, res, next) => {
  try {
    if (req.session.userId) {
      const { userId } = req.session;
      const userExist = await User.findOne({ id: userId });
      if (userExist) {
        return res.redirect('/');
      }
    }
  } catch (error) {
    return next(error);
  }

  const data = prepareLoginTemplate();
  return res.render('login', data);
});

router.get('/forget-password', (req, res, next) => {
  const data = prepareLoginTemplate();
  return res.render('forget-password', data);
});

async function getDevUser(id) {
  const languages = ['Русский', 'Английский'];
  let user;
  try {
    user = new User({ id, role: id, languages });
    await user.save();
    // eslint-disable-next-line no-empty
  } catch (error) {
    logger.error(error);
  }
  if (!user) {
    user = await User.findOne({ id });
  }

  return user;
}

router.post('/login', async (req, res, next) => {
  try {
    const { login: id, password } = req.body;
    let user;
    if (isDevelopment()) {
      user = await getDevUser(id);
    } else {
      user = await User.validate(id, password);
    }

    req.session.userId = user.id;
    req.session.role = user.role;
    return res.send({});
  } catch (error) {
    return next(error);
  }
});

function destroySession(req) {
  return new Promise((resolve, reject) => {
    req.session.destroy((err) => {
      if (err) {
        reject(err);
      }
      resolve();
    });
  });
}

router.get('/logout', async (req, res, next) => {
  try {
    await destroySession(req);
    return res.redirect('/');
  } catch (error) {
    return next(error);
  }
});

export default router;
