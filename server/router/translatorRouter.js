import { Router } from 'express';
import { prepareTranslatorTemplate } from 'server/utils';
import { Order, User } from 'server/mongodb';

const router = new Router();

router.get('/translator/orders/new', async (req, res, next) => {
  const orders = await Order.find({ type: 'translating-ready' });

  const data = prepareTranslatorTemplate({ orders });
  res.render('translator/neworders', data);
});

router.get('/translator/orders', async (req, res, next) => {
  const { id } = req.session;
  const orders = await Order.find({ userId: id });

  const data = prepareTranslatorTemplate({ orders });
  res.render('translator/ordersinwork', data);
});

router.get('/translator', async (req, res, next) => {
  const { userId } = req.session;
  const user = await User.findOne({ id: userId });

  const data = prepareTranslatorTemplate({ user });
  console.log(data);
  res.render('translator/translator', data);
});

export default router;
