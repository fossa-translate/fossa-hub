import orderRoutes from 'server/router/api/orderAPIRouter';
import printRoutes from 'server/router/api/printAPIRouter';
import userRoutes from 'server/router/api/userAPIRouter';
import filesRouter from 'server/router/api/filesAPIRouter';
import mailRouter from 'server/router/api/mailAPIRouter';
import systemRouter from 'server/router/systemRouter';
import operatorRouter from 'server/router/operatorRouter';
import translatorRouter from 'server/router/translatorRouter';
import devRouter from 'server/router/devRouter';

import { Router } from 'express';
import { authorized } from 'server/middlewares';
import { isDevelopment } from 'server/utils';

const router = new Router();

router.use('/api/orders', authorized, orderRoutes);
router.use('/api/print', authorized, printRoutes);
router.use('/api/user', authorized, userRoutes);
router.use('/api/files', authorized, filesRouter);
router.use('/api/mail', authorized, mailRouter);

router.use(systemRouter);
router.use(authorized, operatorRouter);
router.use(authorized, translatorRouter);

if (isDevelopment()) {
  router.use(devRouter);
}

export default router;
