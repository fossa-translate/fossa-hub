import formidableMiddleware from 'express-formidable';

import logger from 'server/logger';
import sentry from 'server/sentry';
import { isOperator } from 'server/utils';
import config from 'config';

export function nocache(req, res, next) {
  res.set('Cache-Control', 'no-store, no-cache, must-revalidate, private');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
}

export function errorHandler(err, req, res, next) {
  logger.error(err.message);
  sentry.captureMessage(err);
  if (err.name === 'NotFoundError') {
    const { message } = err;
    const json = { type: 'error', message };
    res.status(404);
    return res.send(json);
  }

  if (err.name === 'ValidationError') {
    const { message } = err;
    const json = { type: 'error', message };
    res.status(422);
    return res.send(json);
  }
  if (err) {
    const message = 'Error happened! 😠😠😠😠';
    const json = { type: 'error', message };
    res.status(500);
    return res.send(json);
  }
  return next();
}

export function authorized(req, res, next) {
  const { token, headerName } = config.api;
  const validSecretInHeader = req.body.token === token;
  const validSecretInBody = req.headers[headerName] === token;
  const isValid = validSecretInHeader || validSecretInBody || req.session.userId;

  if (isValid) {
    return next();
  }
  return res.redirect('/login');
}

export const formidable = formidableMiddleware({
  encoding: 'utf-8',
  multiples: true,
  uploadDir: 'temp',
  enabledPlugins: ['octetstream', 'querystring', 'json'],
});

export function onlyOperatorMiddleware(req, res, next) {
  if (isOperator(req)) {
    return next();
  }
  return res.sendStatus(401);
}

export function notImplemented(req, res, next) {
  res.status(501);
  res.redirect('/');
}

export function additionalLogger(req, res, next) {
  const { body, params, query } = req;
  if (!body) {
    logger.debug('Body: ', body);
  }
  if (!params) {
    logger.debug('Params: ', params);
  }
  if (!query) {
    logger.debug('Query: ', query);
  }
  next();
}
