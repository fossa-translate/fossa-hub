export const ORDER_TYPES = [
  'new',
  'translating',
  'translating-ready',
  'approving-ready',
  'approving',
  'additional',
  'done',
  'archive',
  'delete',
];

export const ORDER_TYPES_LOCALIZED = {
  new: 'Новый',
  'translating-ready': 'Готов к переводу',
  translating: 'Переводится',
  'approving-ready': 'Готов к проверке',
  approving: 'Проверяется',
  additional: 'Дополняется',
  done: 'Готов',
  archive: 'В архиве',
  delete: 'Удален',
};

export const QUALIFICATIONS = [
  'novice',
  'translator',
  'qualified',
];

export const QUALIFICATIONS_LOCALIZED = {
  novice: 'Стажер',
  translator: 'Переводчик',
  qualified: 'Квалифицированный переводчик',
};

export const FILES_FOLDER = 'files';
