import fs from 'fs-extra';
import path from 'path';
import EmailTemplates from 'email-templates';
import open from 'open';
import nodemailer from 'nodemailer';

import { isDevelopment, getMD5Hash } from 'server/utils';
import config from 'config';
import logger from './logger';

export function emailRender(template, data) {
  const email = new EmailTemplates({
    views: {
      root: path.resolve('emails/templates'),
      options: {
        extension: 'hbs',
      },
    },
    juice: true,
    juiceResources: {
      preserveImportant: true,
      webResources: {
        relativeTo: path.resolve('emails/css'),
      },
    },
  });
  return email.render(template, data);
}

export async function previewEmail(html) {
  const fileName = `${getMD5Hash()}.html`;
  const tempFolder = 'temp';
  const filePath = path.resolve(tempFolder, fileName);
  await fs.writeFile(filePath, html);
  logger.info('Email preview opened');
  await open(filePath, { app: 'chrome' });
  logger.info('Email preview closed');
}

export async function sendEmail(email, template, data) {
  const html = await emailRender(template, data);

  const { host, user, password } = config.smtp;

  const transporter = nodemailer.createTransport({
    host,
    port: 465,
    secure: true,
    auth: {
      user,
      pass: password,
    },
  });

  if (isDevelopment()) {
    await previewEmail(html);
  } else {
    await transporter.sendMail({
      from: `${data.systemName} ${user}`,
      to: email,
      subject: data.subject,
      html,
    });
  }
}
