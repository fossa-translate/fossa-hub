import log4js from 'log4js';
import config from 'config';

log4js.configure(config.log4js);
const logger = log4js.getLogger('Fossa-hub');

export default logger;
