const browserSync = require('browser-sync');
const nodemon = require('nodemon');
const config = require('config');

nodemon({
  script: 'dist/dist.js',
  ignore: ['files', 'public'],
  ext: 'mustache, ejs, json, js',
});

browserSync.init({
  proxy: `localhost:${config.server.port}`,
  files: ['public/**/*.*'],
  port: 7000,
});
